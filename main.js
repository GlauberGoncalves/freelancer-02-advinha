/*
    imprescindível que seja feito Javascript e HTML. Entregar até sábado.

    a. Seu programa ao ser executado irá gerar um aleatório de 6 dígitos
    b. Perguntar para o usuário: Qual o código do cofre?
    c. Permitir que o usuário digite um código
    d. Checar o código e responder e dependendo da situação uma das mensagens abaixo:

    i. Código correto
    ii. Código inválido, 2 números certos
    iii. Código inválido, 2 números certos e 1 na posição correta.
    e. Assim se o código gerado for 123456 e o usuário digitar 222222 a resposta será: Código inválido, 1 número na posição correta.

*/

var print = console.log


/* função para gerar numeros aleatórios em um rang */

const numeroMaximo = 999999

function geraAleatorio(max){
    return Math.floor(Math.random() * max + 1)
}

function geraCodigo(){
    let codigo = [];
    let maximo = 9;

    for(let i=0; i < 6; i++){
        codigo.push( geraAleatorio(maximo));
    }
    return codigo;
}

/* função faz a checagem da tentativa do usuario */

function acertosTotal(num, codigo ){

	let quantidadeAcertos = 0;
	let certos = []

	num.forEach(n => {
    if(codigo.indexOf(n) != -1){
			if(certos.indexOf(n) == -1){
				quantidadeAcertos++;
				certos.push(n);
			}
		}
	});

	return quantidadeAcertos;
}

function acertosPorPosicao(num, codigo){

	let posicaoCorreta = 0;

	for(let i=0; i < codigo.length; i++){
		if(num[i] == codigo[i]){
			posicaoCorreta++;
		}
	}
	return posicaoCorreta;
}

/* função faz a checagem da tentativa do usuario */

function checarCodigo(num, codigo){

    let certosPosicao = acertosPorPosicao(num, codigo);
    let certosTotal = acertosTotal(num, codigo);
    let casasDecimais = 6;

    if (certosPosicao == casasDecimais){
        document.getElementById("resultado").innerHTML = "Código correto";
    } else if(certosPosicao){
        document.getElementById("resultado").innerHTML = `${certosTotal} numeros certos e ${certosPosicao} na posicao correta`;
    } else {
        document.getElementById("resultado").innerHTML = `${certosTotal} numeros certos`;
    }
}

function textoEmLista(texto){
	let lista = []
    for(let i=0; i < texto.length; i++){
			lista.push(parseInt(texto[i]))
	}
	return lista;
}

var codigo = geraCodigo()

$("#btnChecar").click(() => {
    let numero = document.getElementById("numero").value;
    let numeros = textoEmLista(numero);
    checarCodigo(numeros, codigo);
})